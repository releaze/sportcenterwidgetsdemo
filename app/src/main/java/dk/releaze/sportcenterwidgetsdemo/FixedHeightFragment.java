package dk.releaze.sportcenterwidgetsdemo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment container for wrapping layouts, that includes {@link android.widget.ListView} inside.
 * <p/>
 * Created by gpl on 24.10.14.
 */
public class FixedHeightFragment extends Fragment {
    public static final String TAG = FixedHeightFragment.class.getSimpleName();
    private static final String CHILD_FRAGMENT_TAG = TAG + ".CHILD_FRAGMENT_TAG";
    private static final String EXTRA_CLASS_NAME = TAG + ".EXTRA_CLASS_NAME";
    private static final String EXTRA_BUNDLE = TAG + ".EXTRA_BUNDLE";

    public static FixedHeightFragment newInstance(Class<? extends Fragment> clazz, Bundle bundle) {
        FixedHeightFragment fragment = new FixedHeightFragment();
        Bundle arguments = new Bundle();
        arguments.putString(EXTRA_CLASS_NAME, clazz.getCanonicalName());
        arguments.putBundle(EXTRA_BUNDLE, bundle);
        fragment.setArguments(arguments);
        return fragment;
    }

    private String mChildClassName;
    private Bundle mChildBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null) throw new IllegalStateException("Arguments should't be null");
        if (!arguments.containsKey(EXTRA_CLASS_NAME) ||
                !arguments.containsKey(EXTRA_BUNDLE))
            throw new IllegalStateException("Invalid arguments");
        mChildClassName = arguments.getString(EXTRA_CLASS_NAME);
        mChildBundle = arguments.getBundle(EXTRA_BUNDLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fixed_width, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Fragment fragmentByTag = getChildFragmentManager().findFragmentByTag(CHILD_FRAGMENT_TAG);
        if (fragmentByTag == null) {
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.container,
                            Fragment.instantiate(getActivity(),
                                    mChildClassName,
                                    mChildBundle),
                            CHILD_FRAGMENT_TAG)
                    .commit();
        }
    }
}
