package dk.releaze.sportcenterwidgetsdemo;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import dk.releaze.sportcenter.widgets.LineupFragment;
import dk.releaze.sportcenter.widgets.TournamentTableFragment;

/**
 * Created by gpl on 23.10.14.
 */
public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Adds {@link dk.releaze.sportcenter.widgets.TournamentTableFragment} to container.
     *
     * @param view
     */
    public void onAddTableFragment(View view) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container,
                        FixedHeightFragment.newInstance(TournamentTableFragment.class,
                                TournamentTableFragment.withArgs(47)),
                        TournamentTableFragment.TAG)
                .commit();
    }

    /**
     * Adds {@link dk.releaze.sportcenter.widgets.LineupFragment} to container.
     *
     * @param view
     */
    public void onAddLineupFragment(View view) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container,
                        LineupFragment.newInstance(1723989),
                        LineupFragment.TAG)
                .commit();
    }
}
